addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})
/**
 * Respond with hello worker text
 * @param {Request} request
 */
async function handleRequest(request) {
  console.log(request.cf)
  const url = new URL(request.url);
  const path = url.pathname.split("/");
  let version = path[1] || '19.03.12';

  version = version.replace('.yml', '').replace('.yaml', '');

  return new Response(`build:
  image: docker:${version}
  stage: build
  services:
    - docker:${version}-dind
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY/$CI_PROJECT_PATH:latest .
    - docker push $CI_REGISTRY/$CI_PROJECT_PATH:latest
  `
    ,
    {
      headers: { 'content-type': 'text/plain' },
    })
}
